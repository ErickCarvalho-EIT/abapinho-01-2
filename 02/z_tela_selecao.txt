*&---------------------------------------------------------------------*
*&  Include           Z_tela_selecao
*&---------------------------------------------------------------------*
SELECTION-SCREEN BEGIN OF SCREEN 9999 AS SUBSCREEN.
SELECTION-SCREEN: BEGIN OF BLOCK blk1 WITH FRAME TITLE text-001  .

SELECT-OPTIONS: s_carrid FOR spfli-carrid NO INTERVALS. " MODIF ID so.
SELECT-OPTIONS: s_connid FOR spfli-connid NO INTERVALS.
SELECT-OPTIONS: s_cityfr FOR spfli-cityfrom NO INTERVALS . "NO-EXTENSION.

SELECTION-SCREEN: END   OF BLOCK blk1.
SELECTION-SCREEN END OF SCREEN 9999.