*&---------------------------------------------------------------------*
*&  Include           Z_funcoes_01
*&---------------------------------------------------------------------*

FORM prepare_fieldcatalog .

  
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name = 'SPFLI'
    CHANGING
      ct_fieldcat      = gs_fieldcatalog.

  gs_layout-zebra = 'X'.

ENDFORM.


FORM preenche_spfli .

  SELECT carrid connid countryfr cityfrom
          airpfrom countryto cityto airpto fltime arrtime
    FROM spfli
    INTO CORRESPONDING FIELDS OF TABLE gt_horario_voo
    WHERE carrid IN s_carrid
      AND connid IN s_connid
      AND cityfrom IN s_cityfr .

ENDFORM.


FORM acrescentar_tempo .

  DATA ls_horario_voo LIKE LINE OF gt_horario_voo .

  LOOP AT gt_horario_voo INTO ls_horario_voo .
    ls_horario_voo-arrtime = ls_horario_voo-arrtime + zspfli_atrasos-atraso.
    MODIFY gt_horario_voo FROM ls_horario_voo TRANSPORTING arrtime.
  ENDLOOP .

ENDFORM .