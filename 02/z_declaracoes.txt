*&---------------------------------------------------------------------*
*&  Include           Z_DECLARACOES
*&---------------------------------------------------------------------*

TABLES: zspfli_atrasos, spfli.

DATA: g_container        TYPE scrfname VALUE 'CC_ALV',
      g_custom_container TYPE REF TO cl_gui_custom_container,
      g_grid             TYPE REF TO cl_gui_alv_grid,

      gs_fieldcatalog    TYPE lvc_t_fcat,
      gs_layout          TYPE lvc_s_layo,

      gt_horario_voo     TYPE STANDARD TABLE OF spfli,

      ok_code            LIKE sy-ucomm,
      save_ok            LIKE sy-ucomm.